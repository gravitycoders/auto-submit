<?PHP
//get authorization: gapi.auth.getToken().access_token
$file1 = $argv[1];
$file2 = $argv[2];
$nIterations = $argv[3];
$getResults = isset($argv[4]) ? $argv[4] : false;

$authorization = "ya29.4wCkJ9xRGjZphypjlP88hjbunsIAyMdllRdRYSP_h_fx2YqKP_WdkoxW"; 
$session = ".eJw9kMlugzAARH-l8rkHYkhbkHoIKSEgGYQxELhE1DgsMQ5iKTJR_r0RUnufeXozd3C-9GyogDH2E3sF57oAxh28fAMDpNCCKXFrj7gtsjOOIK68hkLUUM3_KjWfRGpGIonI7hM8nt2O9W0umBj_aOPtysQ_j0m3SqHHqepdClvvs0Sr_XpXs8SSfqgphYIUzKsb4dlbkLgm5nFKEl140YdESzZn3DzkUXWh4rAESTcUsY4LUspQoA07FFdsbY_oOk40dAZn73J2NDvaxssJ8slpbiVa0hnt59IR-CeH8XRS8Zba0eoQtHH1zEpHKOuWaWD9-gXcvCuPX26nYe0.B3hNhg.svDKUDb-jDbzQRFvGIlWXVYHMfw"; //Scade ogni ora, sarebbe possibile salvarla su file e modificarla usando il set-cookie di ogni risposta

function httpReq($url, $method = "GET", $fields_string = "") {
	global $authorization, $session;

	$headers = array(
	    'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:34.0) Gecko/20100101 Firefox/34.0',
	    'Accept: application/json, text/plain, */*',
	    'Accept-Encoding: gzip, deflate',
	    'DNT: 1',
	    'Authorization: ' . $authorization,
	    'Referer: http://zerorobotics.mit.edu/ide/',
	    'Cookie: session=' . $session
	    );

	$ch = curl_init();

	if ($method == "POST") {
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		$headers[] = "Content-Type: application/json;charset=utf-8";
		$headers[] = "Content-Length: " . strlen($fields_string);
	}

	//set the url, number of POST vars, POST data
	curl_setopt($ch,CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);

	//execute post
	$result = curl_exec($ch);

	//close connection
	curl_close($ch);

	return $result;
}

function simulation ($code1, $code2) {
	$url = 'http://zerorobotics.mit.edu/ide/simulate';
	$array = array(
		'gameId' => 10,
		'snapshot1' => 94857,
		'snapshot2' => 203458,
		'simConfig' => array(
			'timeout' => 252,
			'state1' => array(0, 0.6, 0, 0, 0.6, 0),
			'state2' => array(0, -0.6, 0, 0, -0.6, 0),
			'gameVariables' => array()
			),
		'code1' => $code1,
		'code2' => $code2
		);
	$fields_string = json_encode($array);
	return httpReq($url, "POST", $fields_string);
}

function getResults ($id) {
	$url = "http://zerorobotics.mit.edu/ide/simulation/" . $id . "/";
	$html = httpReq($url);
	preg_match("/(simData = ')(.*)(';<\/script>)/", $html, $matches);
	$obj = json_decode($matches[2], true);
	return $obj['satData'][0]['dF'][0][count($obj['satData'][0]['dF'][0]) - 1] . " : " . $obj['satData'][1]['dF'][0][count($obj['satData'][1]['dF'][0]) - 1];
}

$h = fopen($file1, "r");
$code1 = fread($h, filesize($file1));
fclose($h);

$h = fopen($file2, "r");
$code2 = fread($h, filesize($file2));
fclose($h);

for ($i = 0; $i < $nIterations; $i++) {
	$id = simulation($code1, $code2);
	echo "Iterazione $i\n";
	echo "Inizio attesa...";

	$obj['status'] = "SIMULATING";
	
	while ($obj['status'] == "SIMULATING") {
		$json = httpReq("http://zerorobotics.mit.edu/ide/simulate/status/" . $id);
		$obj = json_decode($json,1);
		sleep(2);
	}

	if ($obj['status'] == "FAILED") {
		echo "COMPILAZIONE FALLITA: \n";
		echo $obj['message'] . "\n\n";
		break;
	} else {
		echo "Simulazione eseguita: http://zerorobotics.mit.edu/ide/simulation/" . $id . "\n";
		if ($getResults) {
			echo "\t" . getResults($id) . "\n";
		}
	}
}

?>